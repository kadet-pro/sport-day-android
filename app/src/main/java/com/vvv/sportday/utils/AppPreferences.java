package com.vvv.sportday.utils;

import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.vvv.sportday.model.AppTokens;
import com.vvv.sportday.model.PushToken;
import com.vvv.sportday.model.User;

public final class AppPreferences {
    public AppPreferences(Context c) {
        Hawk.init(c).build();
    }

    public AppTokens getAppTokens() {
        return Hawk.get("AppTokens", null);
    }

    public void setAppTokens(AppTokens appTokens) {
        Hawk.put("AppTokens", appTokens);
    }

    public long getLastTokenUpdate() {
        return Hawk.get("lastTokenUpdate", 0L);
    }

    public void setLastTokenUpdate(long lastTokenUpdate) {
        Hawk.put("lastTokenUpdate", lastTokenUpdate);
    }

    public User getUser() {
        return Hawk.get("User", null);
    }

    public void setUser(User user) {
        Hawk.put("User", user);
    }

    public PushToken getPushToken() {
        return Hawk.get("PushToken", null);
    }

    public void setPushToken(PushToken pushToken) {
        Hawk.put("PushToken", pushToken);
    }
}
