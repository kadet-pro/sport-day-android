package com.vvv.sportday;

import android.app.Application;
import android.content.Context;

import com.vvv.sportday.injection.AppComponent;
import com.vvv.sportday.injection.ApplicationModule;

public class SportDayApp extends Application {
    public static Context applicationContext;
    public static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        component = DaggerAppComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    public static Context getContext() {
        return applicationContext;
    }

}
