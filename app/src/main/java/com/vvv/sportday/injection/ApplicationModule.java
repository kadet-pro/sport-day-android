package com.vvv.sportday.injection;

import android.app.Application;
import android.content.Context;

import com.vvv.sportday.BuildConfig;
import com.vvv.sportday.api.API;
import com.vvv.sportday.api.ApiManager;
import com.vvv.sportday.api.SportDayAuthenticator;
import com.vvv.sportday.utils.AppPreferences;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {
    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    public API provideApi() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()// ApiUtils.getHttpsClient()
                .addInterceptor(loggingInterceptor)
                .readTimeout(BuildConfig.API_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(BuildConfig.API_TIMEOUT, TimeUnit.SECONDS)
                .authenticator(new SportDayAuthenticator());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        return retrofit.create(API.class);
    }

    @Provides
    @Singleton
    public AppPreferences provideAppPreferences(Context context) {
        return new AppPreferences(context);
    }

    @Provides
    @Singleton
    public ApiManager provideApiManager(API api, AppPreferences preferences) {
        return new ApiManager(api, preferences);
    }
}
