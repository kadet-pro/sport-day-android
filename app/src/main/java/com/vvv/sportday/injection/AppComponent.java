package com.vvv.sportday.injection;

import com.vvv.sportday.MainActivity;
import com.vvv.sportday.api.API;
import com.vvv.sportday.utils.AppPreferences;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface AppComponent {
    void inject(MainActivity activity);

    AppPreferences getPreferences();

    API getApi();
}
