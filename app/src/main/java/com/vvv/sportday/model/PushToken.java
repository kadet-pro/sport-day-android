package com.vvv.sportday.model;

public class PushToken {
    private String pushToken;
    private boolean isSendToServer = false;

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public boolean isSendToServer() {
        return isSendToServer;
    }

    public void setSendToServer(boolean sendToServer) {
        isSendToServer = sendToServer;
    }
}
