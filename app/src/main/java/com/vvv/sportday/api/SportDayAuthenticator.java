package com.vvv.sportday.api;

import com.vvv.sportday.BuildConfig;
import com.vvv.sportday.SportDayApp;
import com.vvv.sportday.model.AppTokens;
import com.vvv.sportday.utils.AppPreferences;

import java.io.IOException;

import javax.annotation.Nullable;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class SportDayAuthenticator implements Authenticator {

    @Nullable
    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        synchronized (SportDayAuthenticator.class) {
            AppPreferences preferences = SportDayApp.component.getPreferences();
            AppTokens tokens = preferences.getAppTokens();

            if ((System.currentTimeMillis() - preferences.getLastTokenUpdate()) < 1000 * BuildConfig.API_TIMEOUT)
                return response.request().newBuilder().header(BuildConfig.API_HEADER_ACCESS, tokens.getAccessToken()).build();

            retrofit2.Response<AppTokens> resp = SportDayApp.component.getApi().refreshToken(tokens.getRefreshToken()).execute();

            if (resp.isSuccessful() && resp.body() != null) {
                tokens.setAccessToken(resp.body().getAccessToken());
                preferences.setAppTokens(tokens);
                preferences.setLastTokenUpdate(System.currentTimeMillis());
                return response.request().newBuilder().header(BuildConfig.API_HEADER_ACCESS, tokens.getAccessToken()).build();
            }

            return null;
        }
    }
}
