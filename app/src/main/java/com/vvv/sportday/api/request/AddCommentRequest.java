package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class AddCommentRequest {
    private String text;
    @SerializedName("post_id")
    private int postId;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
