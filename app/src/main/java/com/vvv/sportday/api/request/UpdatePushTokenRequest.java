package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class UpdatePushTokenRequest {
    @SerializedName("push_token")
    private String pushToken;
    @SerializedName("device_id")
    private String deviceId;

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
