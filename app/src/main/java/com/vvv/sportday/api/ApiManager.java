package com.vvv.sportday.api;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.vvv.sportday.utils.AppPreferences;

import java.io.ByteArrayOutputStream;
import java.io.File;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ApiManager {
    private static final String TAG = "ApiManager";

    private API api;
    private AppPreferences preferences;

    public ApiManager(API api, AppPreferences preferences) {
        this.api = api;
        this.preferences = preferences;
    }

    public Flowable<BaseResponse<AppTokensResponse>> loginEmail(LoginEmailRequest request) {
        return api.loginEmail(BuildConfig.API_SECRET_TOKEN, request)
                .flatMap(response -> {
                    if (!response.isError() && response.getResponse() != null) {
                        AppTokens tokens = new AppTokens();
                        tokens.copy(response.getResponse());
                        preferences.setAppTokens(tokens);
                    }
                    return Flowable.just(response);
                });
    }

    public Flowable<BaseResponse<AppTokensResponse>> registrationEmail(RegistrationEmailRequest request) {
        return api.registrationEmail(BuildConfig.API_SECRET_TOKEN, request)
                .flatMap(response -> {
                    if (!response.isError() && response.getResponse() != null) {
                        AppTokens tokens = new AppTokens();
                        tokens.copy(response.getResponse());
                        preferences.setAppTokens(tokens);
                    }
                    return Flowable.just(response);
                });
    }

    public Flowable<BaseResponse<LoginFacebookResponse>> loginFacebook(LoginFacebookRequest request) {
        return api.loginFacebook(BuildConfig.API_SECRET_TOKEN, request)
                .flatMap(response -> {
                    if (!response.isError() && response.getResponse() != null) {
                        AppTokens tokens = new AppTokens();
                        tokens.copy(response.getResponse());
                        preferences.setAppTokens(tokens);
                    }
                    return Flowable.just(response);
                });
    }

    public Flowable<BaseResponse> updateUser(UpdateUserRequest request) {
        return api.updateUser(getAccessToken(), request)
                .flatMap(response -> {
                    if (!response.isError()) {
                        User user = new User();
                        user.copy(request);
                        preferences.setUser(user);
                    }
                    return Flowable.just(response);
                });
    }

    @SuppressLint("CheckResult")
    public void updatePushToken() {
        PushToken pushToken = preferences.getPushToken();
        api.updatePushToken(getAccessToken(), new UpdatePushTokenRequest(pushToken.getPushToken()))
                .flatMap(response -> {
                    if (!response.isError()) {
                        pushToken.setSendToServer(true);
                        preferences.setPushToken(pushToken);
                    }
                    return Flowable.just(response);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> Log.d(TAG, "updatePushToken: " + response.isError()), Throwable::printStackTrace);
    }

    public Flowable<BaseResponse<UploadFileResponse>> uploadFile(File file) {
        Bitmap bmp = BitmapFactory.decodeFile(file.getPath());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bos.toByteArray());
        MultipartBody.Part body = MultipartBody.Part.createFormData("photo", file.getName(), requestFile);
        return api.uploadFile(getAccessToken(), body);
    }

    public Flowable<BaseResponse> getUser() {
        return api.getUser(getAccessToken())
                .flatMap(response -> {
                    if (!response.isError() && response.getResponse() != null) {
                        User user = new User();
                        user.copy(response.getResponse());
                        preferences.setUser(user);
                    }
                    return Flowable.just(response);
                });
    }

    private String getAccessToken() {
        if (preferences != null && preferences.getAppTokens() != null && preferences.getAppTokens().getAccessToken() != null)
            return preferences.getAppTokens().getAccessToken();
        else return " ";
    }
}
