package com.vvv.sportday.api.request;

public class RemoveWeightRequest {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
