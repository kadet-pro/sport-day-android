package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class RegistrationEmailRequest {

    private String email;
    private String password;
    @SerializedName("device_id")
    private String deviceId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
