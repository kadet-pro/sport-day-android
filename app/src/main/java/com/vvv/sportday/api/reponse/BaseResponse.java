package com.vvv.sportday.api.reponse;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("error_code")
    private int errorCode;

    @SerializedName("message")
    private String message;
}
