package com.vvv.sportday.api.reponse;

import com.google.gson.annotations.SerializedName;
import com.vvv.sportday.model.AppTokens;

public class RegistrationEmailRespose {
    @SerializedName("tokens")
    private AppTokens tokens;

    public AppTokens getTokens() {
        return tokens;
    }

    public void setTokens(AppTokens tokens) {
        this.tokens = tokens;
    }
}
