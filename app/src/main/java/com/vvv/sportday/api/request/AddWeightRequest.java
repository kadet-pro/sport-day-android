package com.vvv.sportday.api.request;

public class AddWeightRequest {
    private int weight;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
