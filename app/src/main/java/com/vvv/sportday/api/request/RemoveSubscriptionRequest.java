package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class RemoveSubscriptionRequest {
    @SerializedName("user_id_for_removing_from_subscription")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
