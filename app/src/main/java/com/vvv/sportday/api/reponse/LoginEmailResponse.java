package com.vvv.sportday.api.reponse;

import com.google.gson.annotations.SerializedName;
import com.vvv.sportday.model.AppTokens;
import com.vvv.sportday.model.User;

public class LoginEmailResponse {
    @SerializedName("tokens")
    private AppTokens tokens;

    @SerializedName("user")
    private User user;

    public AppTokens getTokens() {
        return tokens;
    }

    public void setTokens(AppTokens tokens) {
        this.tokens = tokens;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
