package com.vvv.sportday.api.request;

import java.util.ArrayList;

public class AddPostRequest {

    private String text;
    private String tags;
    private ArrayList<String> images;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
