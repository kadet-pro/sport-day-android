package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class RemovePostRequest {
    @SerializedName("post_id")
    private int postId;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
