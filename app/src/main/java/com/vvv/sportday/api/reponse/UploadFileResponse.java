package com.vvv.sportday.api.reponse;

import com.google.gson.annotations.SerializedName;

public class UploadFileResponse {
    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
