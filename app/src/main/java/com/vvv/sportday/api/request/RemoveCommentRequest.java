package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class RemoveCommentRequest {
    @SerializedName("comment_id")
    private int commentId;
    @SerializedName("post_id")
    private int postId;

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
