package com.vvv.sportday.api.request;

import com.google.gson.annotations.SerializedName;

public class LoginFacebookRequest {
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("device_id")
    private String deviceId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
