package com.vvv.sportday.api;

import com.vvv.sportday.BuildConfig;
import com.vvv.sportday.api.reponse.BaseResponse;
import com.vvv.sportday.api.reponse.LoginEmailResponse;
import com.vvv.sportday.api.reponse.LoginFacebookResponse;
import com.vvv.sportday.api.reponse.RegistrationEmailRespose;
import com.vvv.sportday.api.reponse.UploadFileResponse;
import com.vvv.sportday.api.request.AddCommentRequest;
import com.vvv.sportday.api.request.AddPostRequest;
import com.vvv.sportday.api.request.AddSubscriptionRequest;
import com.vvv.sportday.api.request.AddWeightRequest;
import com.vvv.sportday.api.request.LoginFacebookRequest;
import com.vvv.sportday.api.request.PostLikeRequest;
import com.vvv.sportday.api.request.RegistrationEmailRequest;
import com.vvv.sportday.api.request.LoginEmailRequest;
import com.vvv.sportday.api.request.RemoveCommentRequest;
import com.vvv.sportday.api.request.RemovePostRequest;
import com.vvv.sportday.api.request.RemoveSubscriptionRequest;
import com.vvv.sportday.api.request.UpdatePushTokenRequest;
import com.vvv.sportday.model.AppTokens;
import com.vvv.sportday.model.Comment;
import com.vvv.sportday.model.Post;
import com.vvv.sportday.model.User;
import com.vvv.sportday.model.Hobby;
import com.vvv.sportday.model.Weight;

import java.util.ArrayList;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface API {


    @POST("auth/refresh_token")
    Call<AppTokens> refreshToken(@Header(BuildConfig.API_HEADER_REFRESH) String refreshToken);

    @POST("auth/login_email")
    Single<LoginEmailResponse> loginEmail(@Header(BuildConfig.API_HEADER_SECRET) String secret, @Body LoginEmailRequest request);

    @POST("auth/registration_email")
    Single<RegistrationEmailRespose> registrationEmail(@Header(BuildConfig.API_HEADER_SECRET) String secret, @Body RegistrationEmailRequest request);

    @POST("auth/login_facebook")
    Single<LoginFacebookResponse> loginFacebook(@Header(BuildConfig.API_HEADER_SECRET) String secret, @Body LoginFacebookRequest request);


    @Multipart
    @POST("file/upload")
    Single<UploadFileResponse> uploadFile(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Part MultipartBody.Part file);


    @POST("user/update")
    Single<BaseResponse> updateUser(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body User request);

    @POST("user/update_push_token")
    Single<BaseResponse> updatePushToken(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body UpdatePushTokenRequest request);

    @GET("user/user")
    Single<User> getUser(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken);

    @POST("user/add_subscription")
    Single<BaseResponse> addSubscription(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body AddSubscriptionRequest request);

    @POST("user/remove_subscription")
    Single<BaseResponse> removeSubscription(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body RemoveSubscriptionRequest request);

    @GET("user/weight")
    Single<ArrayList<Weight>> getWeights(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken);

    @POST("user/add_weight")
    Single<Weight> addWeight(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body AddWeightRequest request);

    @POST("user/remove_weight")
    Single<BaseResponse> removeWeight(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body RemoveSubscriptionRequest request);

    @GET("user/find_user")
    Single<ArrayList<User>> findUser(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Query("name") String name, @Query("from") int from, @Query("step") int step);


    @GET("sp/hobby")
    Single<ArrayList<Hobby>> getHobbys(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken);


    @POST("post/add_post")
    Single<Post> addPost(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body AddPostRequest request);

    @POST("post/remove_post")
    Single<BaseResponse> removePost(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body RemovePostRequest request);

    @POST("post/add_comment")
    Single<Comment> addPostComment(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body AddCommentRequest request);

    @POST("post/remove_comment")
    Single<BaseResponse> removePostComment(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body RemoveCommentRequest request);

    @POST("post/add_like")
    Single<BaseResponse> addPostLike(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body PostLikeRequest request);

    @POST("post/remove_like")
    Single<BaseResponse> removePostLike(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Body PostLikeRequest request);

    @GET("post/comments")
    Single<ArrayList<Comment>> getPostComments(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Query("post_id") int postId);

    @GET("post/user_posts")
    Single<ArrayList<Post>> getUserPosts(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Query("user_id") int userId, @Query("from") int from, @Query("step") int step);

    @GET("post/feed")
    Single<ArrayList<Post>> getFeed(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Query("from") int from, @Query("step") int step);

    @GET("post/interesting_users")
    Single<ArrayList<User>> getInterestingUsers(@Header(BuildConfig.API_HEADER_ACCESS) String accessToken, @Query("from") int from, @Query("step") int step);
}